import { LitElement, html } from 'lit-element';  
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {   

  static get properties(){

    return {
            people: {type: Array}
    }
}

  render() { 
    return html`
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <persona-header></persona-header>
      <div class="row">
        <persona-sidebar class="col-2" @new-person="${this.newPerson}" @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"></persona-sidebar>
        <persona-main class="col-10" @updated-people="${this.updatedPeople}"></persona-main>
        
      </div>
      <persona-footer></persona-footer>
      <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>

  `;
  } 

  updated(changedProperties){
    console.log("updated en persona-app");

    if (changedProperties.has("people")) {
      console.log("ha cambiado el valor de people en persona-app");
      
      this.shadowRoot.querySelector("persona-stats").people = this.people;

    }
  }

  newMaxYearsInCompanyFilter(e){
    console.log("newMaxYearsInCompanyFilter en persona-app");

    this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;




  }

  newPerson(e){
    console.log("newPerson en persona-app");
    this.shadowRoot.querySelector("persona-main").showPersonForm = true;
  }

  updatedPeopleStats(e){
    console.log("updatedPeopleStats");
    this.shadowRoot.querySelector("persona-sidebar").peopleStats  = e.detail.peopleStats;
    this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;

  }

  updatedPeople(e){
    console.log("updatedPeople");
    this.people = e.detail.people;
  }  
} 

customElements.define('persona-app', PersonaApp) 