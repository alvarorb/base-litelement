import { LitElement, html } from 'lit-element';  

class PersonaSidebar extends LitElement { 

  static get properties () {
    return {
      peopleStats: {type: Object}
    }
  }
  
  constructor(){
    super();
    this.peopleStats = {};
  }
  
  render() { 

    return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <aside>
        <section>
            <div class="mt-5">
                <button class="w-100 btn btn-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
            </div>
            <div class="mt-5">
              <input type="range" 
                min="0" 
                max="${this.peopleStats.maxYearsInCompany}" 
                step="1" 
                value="${this.peopleStats.maxYearsInCompany}" 
                @input="${this.updateMaxYearsInCompanyFilter}"/>
            </div>
            <div class="mt-5">
            Número de personas:<span class="badge badge-secondary">${this.peopleStats.numberOfPeople}</span>
            </div>
        </section>        
      </aside>
    `;
  } 

  updateMaxYearsInCompanyFilter(e){
    console.log("updateMaxYearsInCompanyFilteren persona-sidebar");
    console.log("El range vale " + e.target.value);

    this.dispatchEvent(
      new CustomEvent("updated-max-years-filter",
        {
          detail: {
            maxYearsInCompany: e.target.value
          }
        }
      )
    );
  }

  newPerson(){
    console.log("newPerson en persona-sidebar");
    console.log("Se va a crear una persona nueva");
    this.dispatchEvent(new CustomEvent("new-person", {}));
  }
  
} 

customElements.define('persona-sidebar', PersonaSidebar) 